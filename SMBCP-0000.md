# SMBCP-0000
This is the Seattle Matrix Project's first [BCP](https://en.wikipedia.org/wiki/Best_current_practice) (Best Current Practice) which denotes how a BCP is create and propose BCP.

 #### 1 
 All BCP's should be named SMBCP-XXXX with XXXX incrementing by 1 automatically for each new Best Practice Introduced.  
##### 1.1 
The Number will not increment when  a change is made to a BCP.
 #### 2
 BCP's should follow the template in TBD
 #### 3
 All SMBCP proposals and updates shall be submitted as Merge Requests to [this](https://gitlab.com/seattlematrix/best-practices) Repository 


