# Best Practices

A place to put RFC's for Best practices of the Seattle Matrix project

Please read [SMBCP-000](https://gitlab.com/seattlematrix/best-practices/-/blob/main/SMBCP-0000.md) to get started.