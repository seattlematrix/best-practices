# SMBCP-0001 Version Control for Seattle Matrix projects

In order to ensure a safe and consistent Version Control System (VCS) the Seattle Matrix Project has 
adopted git backed by gitlab as our REQUIRED VCS.

#### 1 
All Seattle Matrix projects MUST be version controlled using [git](https://git-scm.com/)
#### 2 
The only source of truth for Seattle Matrix projects will be the repository located at [gitlab](https://gitlab.com/seattlematrix)

#### 3 
All projects MUST have their Default branch set to `main`
#### 4
All changes to any Seattle Matrix projects MUST be done via a Merge or Pull Request to the official repository listed in [SMCP-0001.2](https://gitlab.com/seattlematrix/best-practices/-/blob/main/SMBCP-0001.md#2)
#### 5
In order for a merge request to be accepted it MUST be approved by a member of the Seattle Matrix team who is not the requester or responsible for any of the changes included in the MR
