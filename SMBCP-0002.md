# SMBCP-0002 Building & Deploying code to Seattle Matrix Projects

In order to enable consistancy in building and deploying of projects into the Seattle Matrix 
environment. All projects SHOULD conform to these standards

#### 1 
All project MUST be able to be built and deployed from source using Gitlab-ci pipelines.
#### 2
All pipelines and deployments MUST be idempotent. It should be possible to run the same pipeline multiple times without causing negative impact.

#### 3 
They SHOULD follow the Gitlabs [Continuous integration best practices](https://about.gitlab.com/topics/ci-cd/continuous-integration-best-practices/)
 